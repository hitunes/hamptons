export default function getNodeDimension(elem, axis) {
  elem.style.position = 'fixed';
  elem.style.left = '-1000px';
  elem.style.display = 'flex';
  const dimension = axis == 'height' ? elem.scrollHeight : elem.scrollWidth;
  elem.style.position = 'absolute';
  elem.style.left = '0';
  elem.style.display = 'none';

  return dimension;
}
