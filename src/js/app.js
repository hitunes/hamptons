import "./search";
import "./dropdown";
import "./animate-header";
import "./nav";
const slider = () => {
  $(".carousel").slick({
    dots: true,
    slidesToShow: 1,
    infinite: true,
    speed: 1000,
    prevArrow: false,
    nextArrow: false,
    autoplay: true,
    fade: true,
    autoplaySpeed: 1200,
    cssEase: "linear"
  });
  $(".js-partner-slide").slick({
    slidesToShow: 6,
    slidesToScroll: 3,
    infinite: true,
    speed: 300,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 2500,
    responsive: [
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
    ],
  });
  $(".js-tech-partner-slide").slick({
    slidesToShow: 4,
    slidesToScroll: 3,
    infinite: true,
    speed: 300,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 2000,
    responsive: [
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
    ],
  });
  $(".js-acc-partner-slide").slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    infinite: true,
    speed: 300,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 2200,
    responsive: [
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
    ],
  });
};
$(document).ready(() => {
  $.localScroll({
    target: window,
    queue: true,
    duration: 1000,
    hash: true,
    offset: -100,
  });
    slider();
  
  const contactForm = $(".js-contact-form");
  if (contactForm.length > 0) {
    contactForm.parsley().on("form:submit", () => false);
    contactForm.parsley().on("form:success", () => {
      const customerDetails = {
        first_name: $('input[name="first_name"]').val(),
        last_name: $('input[name="last_name"]').val(),
        email: $('input[name="email"').val(),
        comments: $('textarea[name="comments"]').val(),
      };
    });
  }
});
