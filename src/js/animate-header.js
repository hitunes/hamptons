import './_util';

import dropDown from './dropdown';
import slideOut from './slideout';

// Helper function
function getDropDown(elem) {
  return elem.querySelector('.drop-down');
}

function activateLinks() {
  const navDropdowns = [
    ...document.querySelectorAll('.menu--primary .has-drop-down'),
  ];

  // Attach a dropdown animation element to each single dropdown lement
  // a dropdown element is identified wit the .has-drop-down class
  if (window.outerWidth > 1280) {
    navDropdowns.forEach((element) => {
      const elementLink = element;
      const animated = new animateDropDown(element);
      elementLink.on('mouseover', animated.reveal);
      elementLink.on('mouseleave', animated.hide);
      window.onresize = () => {
        resetStyles();
        activateLinks();
      };
    });
  }
}

activateLinks();

function resetStyles() {
  const navLinks = document.querySelector('.nav__links');
  resetNodes(navLinks);
}

function resetNodes(elem) {
  // clearStyle(elem);
  if (elem.hasChildNodes()) {
    const nodes = [...elem.children].forEach(e => resetNodes(e));
  }
}

function clearStyle(element) {
  // element.removeAttribute('style');
}

function animateDropDown(elem) {
  if (window.innerWidth > 1024) {
    return slideOut(elem);
  }
  return dropDown(elem);
}
