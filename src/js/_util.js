Node.prototype.addClass = function (cssClass) {
  this.classList.add(cssClass);
};

Node.prototype.removeClass = function (cssClass) {
  this.classList.remove(cssClass);
};

Node.prototype.hasClass = function (cssClass) {
  return this.classList.contains(cssClass);
};

Node.prototype.on = eventListener;
window.on = eventListener;

function eventListener(type, listener) {
  this.addEventListener(type, listener);
}

Node.prototype.toggle = function (cssClass) {
  this.classList.toggle(cssClass);
};

Node.prototype.splitText = function (splitString = '', elem = 'span', htmlClass = 'split') {
  const splitText = this.innerHTML
    .split(splitString)
    .map(t => `<${elem} class="${htmlClass}">${t}</${elem}>`);
  this.innerHTML = splitText.join('');
};
