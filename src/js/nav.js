import { TweenMax, TimelineMax, Power3, Bounce } from 'gsap';
import getNodeDimension from './helpers/get-node-dimension';

const hamburgerBtn = document.querySelector('.js-hamburger');
const menuEl = document.querySelector('.menu-wrapper');
const primaryMenuItems = Array.from(document.querySelectorAll('.menu--primary .menu-item'));

const menuActions = Array.from(document.querySelectorAll('.menu-action'));
const secondaryMenuItems = Array.from(
  document.querySelectorAll('.menu--secondary .menu-item'),
).reverse();
const searchBtn = document.querySelector('.js-search');
const submitSearchBtn = document.querySelector('.js-submit-search');

// STATE
let isMenuOpen = false;
let isSearchActive = false;

// Navigation Elements that will fade in
const fadeElList = [menuActions[0], ...secondaryMenuItems, menuActions[1]].reverse();

function showMenu() {
  isMenuOpen = !isMenuOpen;

  hamburgerBtn.classList.toggle('open');
  document.body.classList.toggle('overflow-hidden');

  const menuTl = new TimelineMax();

  if (isMenuOpen) {
    menuTl
      .to(menuEl, 0.4, { y: 0 })
      .staggerTo(
        primaryMenuItems,
        0.3,
        {
          opacity: 1,
          scale: 1,
          y: 0,
          ease: Power3.easeOut,
        },
        0.1,
      )
      .staggerTo(fadeElList, 0.3, { opacity: 1, ease: Power3.easeOut }, 0.1);
  } else {
    menuTl
      .to(menuEl, 0.2, { y: '-125vh' }, 'menuEl')
      .to(primaryMenuItems, 0.1, { opacity: 0, scale: 0.9, y: -16 }, 'menuEl-=0.1')
      .to(fadeElList, 0.1, { opacity: 0 }, 'menuEl-=0.1');
  }
  menuEl.querySelector('.drop-down').style.maxHeight = null;
}

if (!(window.outerWidth > 1280)) {
  hamburgerBtn.addEventListener('click', showMenu);
}

// Handle mobile nav on mobile
if (window.outerWidth < 1280) {
  menuEl.addEventListener('click', e => {
    let link = e.target;
    let dropdownEl = link.nextElementSibling;

    if (!link.localName === 'a' || link.nextElementSibling == null) return;

    e.preventDefault();

    if (dropdownEl.classList.contains('fire-link')) {
      window.location.href = link.attributes.href.value;
    }

    const dropdowns = menuEl.querySelectorAll('.drop-down.active');

    // If dropdown isn't showing
    if (!dropdownEl.classList.contains('drop-down active')) {
      // hide any currencly expanded dropdown
      dropdowns.forEach(dropdown => {
        dropdown.style.maxHeight = null;
        dropdown.classList.remove('active');
        dropdown.classList.remove('fire-link');
      });

      dropdownEl.classList.add('active');
      dropdownEl.style.maxHeight = `${dropdownEl.scrollHeight}px`;
      dropdownEl.classList.add('fire-link');
    }
  });
}

// Handle Tooltip
const quickHelpBtn = menuActions[0];
const tooltip = document.querySelector('.tooltip');

quickHelpBtn.addEventListener('click', toggleTooltip);

document.body.addEventListener('click', e => {
  if (e.target.classList.contains('button')) return;

  document.body.classList.remove('tooltip-visible');
  tooltip.classList.remove('show-tooltip');
});

function toggleTooltip() {
  if (tooltip.classList.contains('show-tooltip')) {
    document.body.classList.remove('tooltip-visible');
    tooltip.classList.remove('show-tooltip');
  } else {
    document.body.classList.add('tooltip-visible');
    tooltip.classList.add('show-tooltip');
  }
}
