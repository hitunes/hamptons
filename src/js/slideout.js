import { TimelineMax } from 'gsap';

import getNodeDimension from './helpers/get-node-dimension';

// Helper function
function getDropDown(elem) {
  return elem.querySelector('.drop-down');
}

export default function slideOut(elem) {
  let dropDown = getDropDown(elem);
  let list = dropDown.querySelectorAll('.menu-item');
  let height = getNodeDimension(dropDown, 'height');

  // Hack!!!
  // height = 'max-content';

  let t = new TimelineMax({
    yoyo: true,
    reversed: true,
    paused: true,
    smootChildTiming: true,
  });

  t.set(dropDown, {
    display: 'flex',
  });
  t.fromTo(
    dropDown,
    0.3,
    {
      autoAlpha: 0,
      height: 0,
      // zIndex: -8,
      yScale: -100,
    },
    {
      height: height,
      autoAlpha: 1,
      yScale: 0,
    },
  );
  t.staggerFromTo(
    list,
    0.6,
    {
      autoAlpha: 0,
      yPercent: 100,
    },
    {
      autoAlpha: 1,
      yPercent: 0,
    },
    '+=0.15',
  );

  function reveal() {
    t.timeScale(1);
    t.play();
  }

  function hide() {
    t.timeScale(2);
    t.reverse();
  }

  return {
    reveal,
    hide,
    t,
  };
}
