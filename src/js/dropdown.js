import { TimelineMax } from 'gsap';

import getNodeDimension from './helpers/get-node-dimension';

function getDropDown(elem) {
  return elem.querySelector('.drop-down');
}

export default function navDropDown(elem) {
  let dropDownItem = getDropDown(elem);
  let navMenu = document.querySelectorAll('.menu--primary');

  let height = getNodeDimension(dropDownItem, 'height');

  // Hack!!!
  height = 'max-content';

  // Dropdown animation
  let t = new TimelineMax({
    yoyo: true,
    paused: true,
    smoothChildTiming: true,
    onStart() {
      TweenMax.set(navMenu, {
        overflow: 'visible',
      });
    },
    onReverseComplete() {
      TweenMax.set(dropDownItem, {
        height: height,
        display: 'none',
      });
    },
  });

  t.fromTo(
    dropDownItem,
    1.2,
    {
      display: 'block',
      height: 0,
    },
    {
      height: height,
      // autoAlpha: 1,
      ease: Power4.easeOut,
    },
  );

  function reveal() {
    t.play();
  }

  function hide() {
    t.reverse();
  }

  return {
    reveal,
    hide,
    t,
  };
}
