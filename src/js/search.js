import { TimelineMax } from 'gsap';

import getNodeDimension from './helpers/get-node-dimension';

// $('.nav').stick_in_parent();

let isSearchActive = false;

const searchBtn = document.querySelector('.js-search');
const searchIcon = searchBtn.querySelector('.icon');

const submitSearchBtn = document.querySelector('.js-submit-search');
const form = document.querySelector('.search__form');

const searchContainer = document.querySelector('.search-dropdown');
const animateHeaderSearch = (function() {
  const children = form.children;
  const t = new TimelineMax({
    yoyo: true,
    paused: true,
    onStart() {
      document.body.addEventListener('click', enableEsc);
      document.body.addEventListener('keydown', enableEsc);
    },
    onComplete() {
      form.querySelector('input[type="text"]').focus();
    },
    onReverseComplete() {
      document.body.removeEventListener('click', enableEsc);
      document.body.removeEventListener('keydown', enableEsc);
    },
  });

  let height = getNodeDimension(searchContainer, 'height');
  t.set(searchContainer, {
    display: 'block',
  });
  t.set(children, {
    autoAlpha: 0,
    position: 'relative',
  });

  t.fromTo(
    searchContainer,
    0.6,
    {
      height: 0,
      autoAlpha: 0,
    },
    {
      height: height,
      autoAlpha: 1,
    },
  ).fromTo(
    children,
    0.6,
    {
      yPercent: -20,
    },
    {
      autoAlpha: 1,
      yPercent: 0,
    },
  );

  function reveal() {
    t.timeScale(2);
    t.play();
  }

  function hide() {
    t.timeScale(4);
    t.reverse();
  }

  function enableEsc(e) {
    let paths = [...e.path];
    let clickedSearchContainer = paths.some(elem => elem == searchContainer);

    if (!clickedSearchContainer || e.key == 'Escape') {
      isSearchActive = false;
      handleSearch(e);
      hide();
    }
    e.stopPropagation();
  }

  return {
    reveal,
    hide,
  };
})();

function handleSearch(e) {
  e.preventDefault();

  if (isSearchActive) {
    animateHeaderSearch.reveal();

    setTimeout(() => {
      searchIcon.classList.remove('icon-search');
      searchIcon.classList.add('icon-x');
      searchContainer.classList.add('open');
    }, 100);
  } else {
    animateHeaderSearch.hide();
    form.reset();

    setTimeout(() => {
      searchIcon.classList.remove('icon-x');
      searchIcon.classList.add('icon-search');
      searchContainer.classList.remove('open');
    }, 100);
  }
}

searchBtn.addEventListener('click', e => {
  isSearchActive = !isSearchActive;

  handleSearch(e);
});
